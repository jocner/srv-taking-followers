import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Follower } from 'src/domain/entities/follower.entity';
import { FollowerController } from './controllers/follower.controller';
import { FollowerService } from './services/follower.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([Follower])
    ],
    providers: [FollowerService],
    controllers: [FollowerController]
})
export class FollowerModule {}
