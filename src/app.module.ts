import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FollowerModule } from './infrastructure/follower.module';
import { env_vars } from './utils/enviroment';



@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: env_vars.CONNECTSTRING,
      useNewUrlParser: true,
      logging: true,
      synchronize: false,
      entities: ['dist/**/**/*.entity{.ts,.js}']
    }),
    FollowerModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
