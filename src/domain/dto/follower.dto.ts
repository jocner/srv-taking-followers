import { ObjectID } from "typeorm";

export class FollowerDTO {
    
    readonly count_follower: string;
    readonly type_envent: string;
    readonly campus: string;
    readonly hour: string;
    readonly date: string;
}

